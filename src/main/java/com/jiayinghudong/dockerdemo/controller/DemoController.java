package com.jiayinghudong.dockerdemo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author gjj
 * @Date 2020-01-21 15:52
 * @Description
 **/
@RestController
@RequestMapping("demo")
@Slf4j
public class DemoController {

    @GetMapping("hello")
    public String sayHi(){
        return "Hello World";
    }
}
