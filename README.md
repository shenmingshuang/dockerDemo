# 发布maven项目的过程 
  1. maven打包  
  2. 删除旧jar包 
  3. docker构建新jar镜像
  4. 推送到仓库中
  5. 启动镜像(删除旧容器，启动新容器)
  
pipeline {
    agent {
        docker {
          image 'maven3.6:v1.0'
          args '-v /usr/share/maven/ref/repository:/usr/share/maven/ref/repository'
        }
    }
    stages {
        stage('Build') {
            steps {
                sh 'mvn clean package -Dmaven.test.skip=true'
            }
        }
        stage('Docker Build & Push') {
            steps {
                sh 'mvn dockerfile:build'
                sh 'mvn dockerfile:push'
            }
        }
    }
}